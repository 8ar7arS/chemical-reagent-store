$('#productImage').bind('change', function () {
    var filename = $("#productImage").val();
    if (/^\s*$/.test(filename)) {
        $(".image-file-upload").removeClass('active');
        $("#productImageChosen").text("No file chosen...");
    } else {
        $(".image-file-upload").addClass('active');
        $("#productImageChosen").text(filename.replace("C:\\fakepath\\", ""));
    }
});

// $('#productPdf').bind('change', function () {
//     var filename = $("#productPdf").val();
//     if (/^\s*$/.test(filename)) {
//         $(".pdf-file-upload").removeClass('active');
//         $("#productPdfChosen").text("No file chosen...");
//     } else {
//         $(".pdf-file-upload").addClass('active');
//         $("#productPdfChosen").text(filename.replace("C:\\fakepath\\", ""));
//     }
// });
//
// $('#articlePdf').bind('change', function () {
//     var filename = $("#articlePdf").val();
//     if (/^\s*$/.test(filename)) {
//         $(".article-pdf-file-upload").removeClass('active');
//         $("#articlePdfChosen").text("No file chosen...");
//     } else {
//         $(".article-pdf-file-upload").addClass('active');
//         $("#articlePdfChosen").text(filename.replace("C:\\fakepath\\", ""));
//     }
// });