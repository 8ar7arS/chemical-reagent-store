package uz.ser7er.chemreagentstore.repository.category;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import uz.ser7er.chemreagentstore.entities.Category;
import uz.ser7er.chemreagentstore.entities.enums.Status;

import java.util.List;
import java.util.Optional;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer> {

    Page<Category> findAllByStatusNot(Status status, Pageable pageable);

    List<Category> findAllByStatusNot(Status status);

    Optional<Category> findByIdAndStatusNot(Integer categoryId, Status status);

    @Query(value = "from Category c where c.status <> 'DELETED' and (:title IS NULL OR LOWER(c.title) LIKE CONCAT('%', LOWER(:title), '%'))")
    Page<Category> getCategoryList(@Param("title") String title, Pageable pageable);

    Optional<Long> countByStatusNot(Status status);
}
