package uz.ser7er.chemreagentstore.repository.chemical;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import uz.ser7er.chemreagentstore.entities.Chemical;
import uz.ser7er.chemreagentstore.entities.enums.Status;
import uz.ser7er.chemreagentstore.entities.enums.UnitType;

import java.math.BigDecimal;
import java.util.Optional;

@Repository
public interface ChemicalRepository extends JpaRepository<Chemical, Integer> {
    Page<Chemical> findAllByStatusNot(Status status, Pageable pageable);

    @Transactional
    Optional<Chemical> findByIdAndStatusNot(Integer id, Status status);

    @Transactional
    @Query(value = "from Chemical ch where (:name is null or lower(ch.name) LIKE CONCAT('%', LOWER(:name), '%'))" +
            " and (:categoryId is null or ch.categoryId = :categoryId)" +
            " and (:manufacturerId is null or ch.manufacturerId = :manufacturerId)" +
            " and (:price is null or ch.price = :price)" +
            " and (:unitType is null or ch.unitType = :unitType)" +
            " and ch.status <> :status")
    Page<Chemical> getChemicalWithFilter(@Param("name") String name,
                                         @Param("categoryId") Integer categoryId,
                                         @Param("manufacturerId") Integer manufacturerId,
                                         @Param("price") BigDecimal price,
                                         @Param("unitType") UnitType unitType,
                                         Status status,
                                         Pageable pageable);

    @Query(value = "select * from chemical ch " +
            " left join category ca on ch.category_id = ca.id\n" +
            " left join manufacturer ma on ch.manufacturer_id = ma.id\n" +
            " where (:name is null or c.name like %:name%)\n" +
            " and (:categoryName is null or ca.title like %:categoryName%)\n" +
            " and (:price is null or ch.price = :price)\n" +
            " and (:unitType is null or ch.unitType = :unitType)\n" +
            " and (:manufacturerName is null or ma.name like %:manufacturerName%)\n" +
            " and ch.status <> 'DELETED' and ca.status <> 'DELETED' and ma.status <> 'DELETED'", nativeQuery = true)
    Page<Chemical> getChemicalListWithFilter(@Param("name") String name,
                                             @Param("categoryName") String categoryName,
                                             @Param("price") BigDecimal price,
                                             @Param("unitType") UnitType unitType,
                                             @Param("manufacturerName") String manufacturerName,
                                             Pageable pageable);

    Optional<Long> countByStatusNot(Status status);

    Page<Chemical> findByStatusNot(Status status, Pageable pageable);
}
