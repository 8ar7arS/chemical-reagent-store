package uz.ser7er.chemreagentstore.repository.user;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import uz.ser7er.chemreagentstore.entities.User;
import uz.ser7er.chemreagentstore.entities.enums.Status;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Integer> {
    Optional<User> findByEmail(String email);

    @Query("from User u where (:userName is null or lower(u.userName) LIKE CONCAT('%', LOWER(:userName), '%')) and u.status <> 'DELETED'")
    Page<User> getList(@Param("userName") String userName, Pageable pageable);

    Optional<User> findByIdAndStatusNot(Integer userId, Status status);
}
