package uz.ser7er.chemreagentstore.repository.manufacturer;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import uz.ser7er.chemreagentstore.entities.Manufacturer;
import uz.ser7er.chemreagentstore.entities.enums.Status;

import java.util.List;
import java.util.Optional;

@Repository
public interface ManufacturerRepository extends JpaRepository<Manufacturer, Integer> {

    Page<Manufacturer> findAllByStatusNot(Status status, Pageable pageable);

    List<Manufacturer> findAllByStatusNot(Status status);

    Optional<Manufacturer> findByNameAndStatusNot(String name, Status status);

    Optional<Manufacturer> findByIdAndStatusNot(Integer id, Status status);

    @Query(value = "from Manufacturer m where m.status <> 'DELETED' and (:name IS NULL OR LOWER(m.name) LIKE CONCAT('%', LOWER(:name), '%'))")
    Page<Manufacturer> getManufacturerList(@Param("name") String name, Pageable pageable);

    Optional<Long> countByStatusNot(Status status);
}
