package uz.ser7er.chemreagentstore.repository.product;

import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.ser7er.chemreagentstore.entities.AttachEntity;

@Repository
public interface AttachRepository extends JpaRepository<AttachEntity, String> {

    @Modifying
    @Transactional
    @Query("delete from AttachEntity where id = ?1")
    void deleteCascade(String id);

    AttachEntity findByOriginalName(String originalName);

}
