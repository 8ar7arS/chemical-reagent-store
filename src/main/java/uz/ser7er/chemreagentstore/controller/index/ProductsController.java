package uz.ser7er.chemreagentstore.controller.index;

import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import lombok.AllArgsConstructor;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import uz.ser7er.chemreagentstore.config.CustomUserDetailsService;
import uz.ser7er.chemreagentstore.dto.CategoryDto;
import uz.ser7er.chemreagentstore.dto.ManufacturerDto;
import uz.ser7er.chemreagentstore.dto.chemical.ChemicalDto;
import uz.ser7er.chemreagentstore.dto.user.UserDto;
import uz.ser7er.chemreagentstore.dto.wrap.DataTable;
import uz.ser7er.chemreagentstore.entities.Category;
import uz.ser7er.chemreagentstore.entities.Chemical;
import uz.ser7er.chemreagentstore.entities.Manufacturer;
import uz.ser7er.chemreagentstore.entities.User;
import uz.ser7er.chemreagentstore.entities.enums.DeliveryStatus;
import uz.ser7er.chemreagentstore.entities.enums.Role;
import uz.ser7er.chemreagentstore.entities.enums.Status;
import uz.ser7er.chemreagentstore.entities.enums.UnitType;
import uz.ser7er.chemreagentstore.repository.user.UserRepository;
import uz.ser7er.chemreagentstore.repository.category.CategoryRepository;
import uz.ser7er.chemreagentstore.repository.chemical.ChemicalRepository;
import uz.ser7er.chemreagentstore.repository.manufacturer.ManufacturerRepository;
import uz.ser7er.chemreagentstore.repository.product.AttachRepository;
import uz.ser7er.chemreagentstore.service.CategoryService;
import uz.ser7er.chemreagentstore.service.ChemicalService;
import uz.ser7er.chemreagentstore.service.ManufacturerService;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Controller
@RequestMapping("/index")
public class ProductsController {
    private final ChemicalRepository chemicalRepository;
    private final CategoryRepository categoryRepository;
    private final ManufacturerRepository manufacturerRepository;
    private final AttachRepository attachRepository;
    private final UserRepository userRepository;
    private final CustomUserDetailsService customUserDetailsService;
    private final JavaMailSender javaMailSender;
    private final ChemicalService chemicalService;
    private final CategoryService categoryService;
    private final ManufacturerService manufacturerService;


//    @GetMapping("/shopping-cart")
//    public String getProducts(Model model) {
//        return "shopping-cart";
//    }

    @GetMapping("/chemicals")
    @PreAuthorize("permitAll()")
    public String getAllProducts(@RequestParam(name = "category", required = false) String category,
                                 @RequestParam(name = "page", defaultValue = "0", required = false) int page,
                                 @RequestParam(name = "size", defaultValue = "12", required = false) Integer size,
                                 @ModelAttribute("filterDto") ChemicalDto filterDto,
                                 Model model) {

        DataTable<ChemicalDto> table = chemicalService.getList(page, size, null, null, filterDto);
        List<CategoryDto> categories = categoryService.getList();
        List<ManufacturerDto> manufacturers = manufacturerService.getList();
        List<UnitType> unitTypeList = Arrays.stream(UnitType.values()).toList();

        model.addAttribute("filterDto", filterDto);
        model.addAttribute("unitTypeList", unitTypeList);
        model.addAttribute("chemicals", table.getData());
        model.addAttribute("currentPage", page);
        model.addAttribute("totalPages", table.getTotalPage());
        model.addAttribute("categories", categories);
        model.addAttribute("manufacturers", manufacturers);
        model.addAttribute("pageSize", size);

        return "index/index";
    }

    @GetMapping("/details/{id}")
    public String getDetails(@PathVariable("id") Integer chemicalId, Model model) {
        ChemicalDto chemicalDto = chemicalService.getById(chemicalId);
        UserDto userDto = new UserDto();
        userDto.setEmail(customUserDetailsService.getUserName());
        if (userDto.getEmail().equalsIgnoreCase("anonymousUser")) return "redirect:/auth/registration";
        model.addAttribute("userDto", userDto);
        model.addAttribute("chemical", chemicalDto);
        model.addAttribute("activeLink", "index");
        return "index/index2";
    }

    @GetMapping("/checkout/{id}")
    public String getCheckout(@PathVariable("id") Integer chemicalId, Model model) {
        ChemicalDto chemicalDto = chemicalService.getById(chemicalId);
        model.addAttribute("chemical", chemicalDto);
        return "index/checkout";
    }

    @GetMapping("/confirmation")
    public String getConfirmation(Model model) {
        return "index/confirmation";
    }

    @PostMapping("/purchase/{id}")
    public String purchase(@RequestParam("email") String email,
                           @RequestParam("phone") String phone,
                           @RequestParam("firstName") String firstName,
                           @RequestParam("lastName") String lastname,
                           @RequestParam("quantity") String quantity,
                           @PathVariable("id") Integer chemicalId) throws MessagingException, UnsupportedEncodingException {
        sendVerificationEmail(email, phone, firstName, lastname, chemicalId, quantity);
        return "index/success";
    }

    private void sendVerificationEmail(String email, String phone, String firstName, String lastName, Integer chemicalId, String quantity)
            throws UnsupportedEncodingException, MessagingException {

        int qty = Integer.parseInt(quantity);
        ChemicalDto chemical = chemicalService.getById(chemicalId);

        String subject = "Thank you for ordering in ChemRegent";
        String senderName = "Sarvar Store";
        String mailContent = "<p><b>Order number:</b> " + "1" + "</p>";
        mailContent += "<p><b>Payment:</b> " + DeliveryStatus.IN_PROGRESS + "</p>";
        mailContent += "<p><b>City:</b> " + "Tashkent" + "</p>";
        mailContent += "<p><b>Address:</b> " + "Tashkent" + "</p>";
        mailContent += "<p><b>Email:</b> " + customUserDetailsService.getUserName() + "</p>";
        mailContent += "<p><b>First name:</b> " + firstName + "</p>";
        mailContent += "<p><b>Last name:</b> " + lastName + "</p>";
        mailContent += "<p><b>Phone:</b> " + phone + "</p>";
        mailContent += "<p><b>Order total:</b> " + qty * chemical.getPrice().intValue() + "</p>";

        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true);

        helper.setFrom("8ar7ar@mail.ru", senderName);
        helper.setTo(email);
        helper.setSubject(subject);
        helper.setText(mailContent, true);

        javaMailSender.send(message);
    }

    @GetMapping("/init")
    public String init() {
        generateUser();
        generateManufacturer();
        generateCategory();
        generateChemical();
        return "redirect:/index/chemicals";
    }

    public void generateUser() {
        Optional<User> optional = userRepository.findByEmail("admin@mail.ur");
        if (optional.isPresent()) {
            throw new RuntimeException("User already exists!");
        }
        User user = new User();
        user.setRole(Role.ROLE_ADMIN);
        user.setUserName("admin");
        user.setPassword(new BCryptPasswordEncoder().encode("123"));
        user.setStatus(Status.ACTIVE);
        user.setEmail("admin@mail.ru");
        user.setUserName("sarvar");
        userRepository.save(user);

    }

    public void generateManufacturer() {
        for (int i = 1; i < 11; i++) {
            Manufacturer manufacturer = new Manufacturer();
            manufacturer.setName("MANUFACTURER-" + i);
            manufacturer.setStatus(Status.ACTIVE);
            manufacturer.setCreatedDate(LocalDateTime.now());
            manufacturer.setCreatedBy(1);
            manufacturerRepository.save(manufacturer);
        }
    }

    public void generateCategory() {
        for (int i = 1; i < 11; i++) {
            Category category = new Category();
            category.setTitle("CATEGORY-" + i);
            category.setStatus(Status.ACTIVE);
            category.setEnabled(true);
            category.setCreatedBy(1);
            category.setCreatedDate(LocalDateTime.now());
            if (i != 1) category.setParentId(1);
            categoryRepository.save(category);
        }
    }

    public void generateChemical() {
        int j = 1;
        for (int i = 1; i < 33; i++) {
            Chemical chemical = new Chemical();
            chemical.setStatus(Status.ACTIVE);
            chemical.setName("CHEMICAL-" + i);
            chemical.setImageUrl("ce4aa2ea-6515-481e-ab37-0c25c9ed1474");
            chemical.setPrice(BigDecimal.valueOf(10 + 2 * i));
            chemical.setDescription("Lorem Ipsum is simply dummy.");
            chemical.setCreatedBy(1);
            chemical.setCreatedDate(LocalDateTime.now());
            chemical.setAvailableAmount(BigDecimal.valueOf(10));
            chemical.setUnitType(UnitType.MG);
            chemical.setManufacturerId(j);
            chemical.setCategoryId(j);
            chemicalRepository.save(chemical);
            if (i == 10 || i == 20 || i == 30) j = 0;
            j++;
        }
    }

}
