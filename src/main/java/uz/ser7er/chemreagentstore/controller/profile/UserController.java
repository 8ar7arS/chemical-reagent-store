package uz.ser7er.chemreagentstore.controller.profile;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import uz.ser7er.chemreagentstore.dto.user.UserDto;
import uz.ser7er.chemreagentstore.dto.wrap.DataTable;
import uz.ser7er.chemreagentstore.entities.enums.Role;
import uz.ser7er.chemreagentstore.service.UserService;

import java.util.Arrays;

@AllArgsConstructor
@Controller
@RequestMapping("/user")
public class UserController {
    private final UserService userService;

    @GetMapping("/list")
    public String getList(@RequestParam(defaultValue = "0") int page,
                          @RequestParam(defaultValue = "5") int size,
                          @ModelAttribute("filterDto") UserDto filterDto,
                          @RequestParam(defaultValue = "id") String sortField,
                          @RequestParam(defaultValue = "asc") String sortDir,
                          Model model) {
        DataTable<UserDto> table = userService.getList(page, size, sortField, sortDir, filterDto);

        model.addAttribute("page", page);
        model.addAttribute("size", size);
        model.addAttribute("currentPage", page);
        model.addAttribute("totalPages", table.getTotalPage());
        model.addAttribute("totalItems", table.getTotalItems());
        model.addAttribute("sortField", sortField);
        model.addAttribute("sortDir", sortDir);
        model.addAttribute("reverseSortDir", sortDir.equalsIgnoreCase("asc") ? "desc" : "asc");
        model.addAttribute("filterDto", filterDto);
        model.addAttribute("users", table.getData());
        model.addAttribute("activeLink", "user");
        return "admin/user/user";
    }

    @GetMapping("/update/{id}")
    public String update(@PathVariable("id") Integer userId,
                         Model model) {
        UserDto userDto = userService.getById(userId);
        if (userDto == null) return "redirect:/dashboard/";
        model.addAttribute("userRole", Arrays.stream(Role.values()).toList());
        model.addAttribute("isUpdate", true);
        model.addAttribute("user", userDto);
        model.addAttribute("activeLink", "user");
        model.addAttribute("");
        return "admin/user/add_user";
    }

    @PostMapping("/edit")
    public String edit(@ModelAttribute UserDto userDto,
                       Model model) {
        Boolean result = userService.edit(userDto);
        model.addAttribute("activeLink", "user");
        if (result) return "redirect:/user/list";
        else return "redirect:/dashboard";
    }

}
