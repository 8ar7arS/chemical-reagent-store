package uz.ser7er.chemreagentstore.controller.attach;

import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import uz.ser7er.chemreagentstore.dto.attach.AttachDTO;
import uz.ser7er.chemreagentstore.service.AttachService;

@AllArgsConstructor
@RestController
@RequestMapping("/attach")
public class AttachController {
    private final AttachService attachService;

    @PostMapping("/upload")
    public ResponseEntity<AttachDTO> create(@RequestParam("file") MultipartFile file) {
        return ResponseEntity.ok(attachService.upload(file));
    }

    @GetMapping(value = "/open/{fileName}", produces = MediaType.IMAGE_PNG_VALUE)
    public byte[] open_general(@PathVariable("fileName") String fileName) {
        return attachService.open_general(fileName);
    }
}
