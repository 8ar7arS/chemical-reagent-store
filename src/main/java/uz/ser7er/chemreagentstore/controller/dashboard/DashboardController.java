package uz.ser7er.chemreagentstore.controller.dashboard;


import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import uz.ser7er.chemreagentstore.config.CustomUserDetails;
import uz.ser7er.chemreagentstore.service.CategoryService;
import uz.ser7er.chemreagentstore.service.ChemicalService;
import uz.ser7er.chemreagentstore.service.ManufacturerService;

@AllArgsConstructor
@Controller
@RequestMapping("/dashboard")
public class DashboardController {
    private final ChemicalService chemicalService;
    private final CategoryService categoryService;
    private final ManufacturerService manufacturerService;

    @GetMapping({"", "/"})
    public String getSuccess(Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        CustomUserDetails principal = (CustomUserDetails) authentication.getPrincipal();

        model.addAttribute("chemicalCount", chemicalService.chemicalCount());
        model.addAttribute("categoryCount", categoryService.categoryCount());
        model.addAttribute("manufacturerCount", manufacturerService.ManufacturerCount());
        model.addAttribute("activeLink", "dashboard");
        model.addAttribute("profileName", principal.getUsername());

        return "admin/dashboard";
    }
}
