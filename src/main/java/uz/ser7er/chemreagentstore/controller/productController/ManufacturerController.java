package uz.ser7er.chemreagentstore.controller.productController;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import uz.ser7er.chemreagentstore.dto.ManufacturerDto;
import uz.ser7er.chemreagentstore.dto.wrap.DataTable;
import uz.ser7er.chemreagentstore.service.ManufacturerService;

@AllArgsConstructor
@Controller
@RequestMapping("/manufacturer")
public class ManufacturerController {
    private final ManufacturerService manufacturerService;

    @GetMapping("/go/add")
    public String goToAdd(Model model) {
        model.addAttribute("categories", manufacturerService.getList());
        model.addAttribute("manufacturerDto", new ManufacturerDto());
        model.addAttribute("isUpdate", false);
        model.addAttribute("activeLink", "manufacturer");
        return "admin/manufacturer/add_manufacturer";
    }

    @PostMapping("/create")
    public String create(@ModelAttribute ManufacturerDto manufacturerDto) {
        manufacturerService.create(manufacturerDto);
        return "redirect:/manufacturer/list";
    }

    @GetMapping("/go/update/{id}")
    public String goToUpdate(@PathVariable("id") Integer manufacturerId,
                             Model model) {
        ManufacturerDto manufacturerDto = manufacturerService.getById(manufacturerId);
        if (manufacturerDto == null) return "admin/manufacturer/manufacturerWithFilter";

        model.addAttribute("manufacturers", manufacturerService.getList());
        model.addAttribute("manufacturerDto", manufacturerDto);
        model.addAttribute("isUpdate", true);
        model.addAttribute("activeLink", "manufacturer");
        return "admin/manufacturer/add_manufacturer";
    }

    @PostMapping("/update")
    public String update(@ModelAttribute("manufacturer") ManufacturerDto manufacturerDto,
                         Model model) {
        Boolean result = manufacturerService.update(manufacturerDto);
        model.addAttribute("activeLink", "manufacturer");
        if (result) return "redirect:/manufacturer/list";
        else return "redirect:/dashboard";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") Integer manufacturerId, Model model) {
        Boolean result = manufacturerService.delete(manufacturerId);
        model.addAttribute("activeLink", "manufacturer");
        if (result) return "redirect:/manufacturer/list";
        else return "redirect:/dashboard";
    }

    @GetMapping("/list")
    public String getList(@RequestParam(defaultValue = "0") int page,
                          @RequestParam(defaultValue = "5") int size,
                          @ModelAttribute ManufacturerDto filterDto,
                          @RequestParam(defaultValue = "id") String sortField,
                          @RequestParam(defaultValue = "asc") String sortDir,
                          Model model) {
        DataTable<ManufacturerDto> table = manufacturerService.getListWithPaging(page, size, sortField, sortDir, filterDto);
        model.addAttribute("filterDto", filterDto);
        model.addAttribute("manufacturers", table.getData());
        model.addAttribute("currentPage", page);
        model.addAttribute("size", size);
        model.addAttribute("totalPages", table.getTotalPage());
        model.addAttribute("totalItems", table.getTotalItems());
        model.addAttribute("activeLink", "manufacturer");
        return "admin/manufacturer/manufacturerWithFilter";
    }
}
