package uz.ser7er.chemreagentstore.controller.productController;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import uz.ser7er.chemreagentstore.dto.CategoryDto;
import uz.ser7er.chemreagentstore.dto.wrap.DataTable;
import uz.ser7er.chemreagentstore.service.CategoryService;

@AllArgsConstructor
@Controller
@RequestMapping("/category")
public class CategoryController {
    private final CategoryService categoryService;

    @GetMapping("/go/add")
    public String goToAdd(Model model) {
        model.addAttribute("categories", categoryService.getList());
        model.addAttribute("categoryDto", new CategoryDto());
        model.addAttribute("isUpdate", false);
        model.addAttribute("activeLink", "category");
        return "admin/category/add_category";
    }

    @PostMapping("/create")
    public String create(@ModelAttribute CategoryDto categoryDto) {
        categoryService.create(categoryDto);
        return "redirect:/category/list";
    }

    @GetMapping("/go/update/{id}")
    public String goToUpdate(@PathVariable("id") Integer categoryId,
                             Model model) {
        CategoryDto categoryDto = categoryService.getById(categoryId);
        if (categoryDto == null) return "admin/category/categoryWithFilter";

        model.addAttribute("categories", categoryService.getList());
        model.addAttribute("categoryDto", categoryDto);
        model.addAttribute("isUpdate", true);
        model.addAttribute("activeLink", "category");
        return "admin/category/add_category";
    }

    @PostMapping("/update")
    public String update(@ModelAttribute("category") CategoryDto categoryDto,
                         Model model) {
        Boolean result = categoryService.update(categoryDto);
        model.addAttribute("activeLink", "category");
        if (result) return "redirect:/category/list";
        else return "redirect:/dashboard";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") Integer categoryId,
                         Model model) {
        Boolean result = categoryService.delete(categoryId);
        model.addAttribute("activeLink", "category");
        if (result) return "redirect:/category/list";
        else return "redirect:/dashboard";
    }

    @GetMapping("/list")
    public String getList(@RequestParam(defaultValue = "0") int page,
                          @RequestParam(defaultValue = "5") int size,
                          @ModelAttribute CategoryDto filterDto,
                          @RequestParam(defaultValue = "id") String sortField,
                          @RequestParam(defaultValue = "asc") String sortDir,
                          Model model) {
        DataTable<CategoryDto> table = categoryService.getListWithPaging(page, size, sortField, sortDir, filterDto);
        model.addAttribute("filterDto", filterDto);
        model.addAttribute("categories", table.getData());
        model.addAttribute("currentPage", page);
        model.addAttribute("size", size);
        model.addAttribute("totalPages", table.getTotalPage());
        model.addAttribute("totalItems", table.getTotalItems());
        model.addAttribute("activeLink", "category");
        return "admin/category/categoryWithFilter";
    }
}
