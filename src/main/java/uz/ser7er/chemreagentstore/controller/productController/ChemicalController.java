package uz.ser7er.chemreagentstore.controller.productController;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import uz.ser7er.chemreagentstore.dto.CategoryDto;
import uz.ser7er.chemreagentstore.dto.ManufacturerDto;
import uz.ser7er.chemreagentstore.dto.chemical.ChemicalDto;
import uz.ser7er.chemreagentstore.dto.product.FileWrapDTO;
import uz.ser7er.chemreagentstore.dto.wrap.DataTable;
import uz.ser7er.chemreagentstore.entities.enums.UnitType;
import uz.ser7er.chemreagentstore.service.CategoryService;
import uz.ser7er.chemreagentstore.service.ChemicalService;
import uz.ser7er.chemreagentstore.service.ManufacturerService;

import java.util.Arrays;
import java.util.List;

@AllArgsConstructor
@Controller
@RequestMapping("/chemical")
public class ChemicalController {
    private final ChemicalService chemicalService;
    private final CategoryService categoryService;
    private final ManufacturerService manufacturerService;

    @GetMapping("/list")
    public String getChemicalList(@RequestParam(defaultValue = "0") int page,
                                  @RequestParam(defaultValue = "12") int size,
                                  @ModelAttribute("filterDto") ChemicalDto filterDto,
                                  @RequestParam(defaultValue = "id") String sortField,
                                  @RequestParam(defaultValue = "asc") String sortDir,
                                  Model model) {
        DataTable<ChemicalDto> table = chemicalService.getList(page, size, sortField, sortDir, filterDto);
        List<CategoryDto> categories = categoryService.getList();
        List<ManufacturerDto> manufacturers = manufacturerService.getList();


        model.addAttribute("manufacturers", manufacturers);
        model.addAttribute("page", page);
        model.addAttribute("size", size);
        model.addAttribute("chemicals", table.getData());
        model.addAttribute("categories", categories);
        model.addAttribute("unitTypeList", Arrays.stream(UnitType.values()).toList());
        model.addAttribute("currentPage", page);
        model.addAttribute("totalPages", table.getTotalPage());
        model.addAttribute("totalItem", table.getTotalItems());
        model.addAttribute("activeLink", "chemical");
        model.addAttribute("filterDto", fillFilterDto(filterDto));
        model.addAttribute("sortField", sortField);
        model.addAttribute("sortDir", sortDir);
        model.addAttribute("reverseSortDir", sortDir.equalsIgnoreCase("asc") ? "desc" : "asc");

        return "admin/chemical/chemicalsWithFilter";
    }

    @GetMapping("/add")
    public String addChemical(Model model) {
        List<CategoryDto> categoryList = categoryService.getList();
        List<ManufacturerDto> manufacturers = manufacturerService.getList();
        List<UnitType> unitTypeList = Arrays.stream(UnitType.values()).toList();
        model.addAttribute("categories", categoryList);
        model.addAttribute("manufacturers", manufacturers);
        model.addAttribute("unitTypeList", unitTypeList);
        model.addAttribute("chemical", new ChemicalDto());
        model.addAttribute("multipart", new FileWrapDTO());
        model.addAttribute("isUpdate", false);
        model.addAttribute("activeLink", "chemical");
        return "admin/chemical/add_chemical";
    }

    @GetMapping("/update/{id}")
    public String updateChemical(@PathVariable("id") Integer chemicalId,
                                 Model model) {
        ChemicalDto chemicalDto = chemicalService.getById(chemicalId);
        if (chemicalId == null) return "admin/dashboard";

        List<CategoryDto> categoryList = categoryService.getList();
        List<ManufacturerDto> manufacturers = manufacturerService.getList();
        List<UnitType> unitTypeList = Arrays.stream(UnitType.values()).toList();
        model.addAttribute("unitTypeList", unitTypeList);
        model.addAttribute("categories", categoryList);
        model.addAttribute("manufacturers", manufacturers);
        model.addAttribute("multipart", new FileWrapDTO());
        model.addAttribute("chemical", chemicalDto);
        model.addAttribute("isUpdate", true);
        model.addAttribute("activeLink", "chemical");
        return "admin/chemical/add_chemical";
    }

    @PostMapping("/edit")
    public String editChemical(@RequestParam("file") MultipartFile file,
                               @ModelAttribute ChemicalDto chemicalDto,
                               Model model) {
        // todo attach upload for chemical
        FileWrapDTO fileWrapDTO = new FileWrapDTO();
        fileWrapDTO.setMultipartFile(file);
        Boolean result = chemicalService.edit(chemicalDto, fileWrapDTO);
        model.addAttribute("activeLink", "chemical");
        model.addAttribute("");
        if (result) return "redirect:/chemical/list";
        else return "redirect:/admin/dashboard";

    }

    @PostMapping("/create")
    public String create(@ModelAttribute ChemicalDto chemicalDto,
                         @RequestParam(name = "file") MultipartFile file,
                         Model model) {
        FileWrapDTO fileWrapDTO = new FileWrapDTO();
        fileWrapDTO.setMultipartFile(file);
        chemicalService.create(chemicalDto, fileWrapDTO);
        model.addAttribute("activeLink", "chemical");
        return "redirect:/chemical/list";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") Integer chemicalId) {
        Boolean result = chemicalService.delete(chemicalId);
        if (result) return "redirect:/chemical/list";
        else return "redirect:/dashboard/";
    }

    public ChemicalDto fillFilterDto(ChemicalDto filterDto) {
        if (filterDto.getCategoryId() != null)
            filterDto.setCategoryTitle(categoryService.getById(filterDto.getCategoryId()).getTitle());

        if (filterDto.getManufacturerId() != null)
            filterDto.setManufacturerName(manufacturerService.getById(filterDto.getManufacturerId()).getName());

        if (filterDto.getUnitTypeId() != null)
            filterDto.setUnitType(UnitType.values()[filterDto.getUnitTypeId()]);

        return filterDto;
    }
}
