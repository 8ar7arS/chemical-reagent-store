package uz.ser7er.chemreagentstore.controller.auth;


import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import uz.ser7er.chemreagentstore.config.CustomUserDetailsService;
import uz.ser7er.chemreagentstore.dto.auth.AuthDTO;
import uz.ser7er.chemreagentstore.entities.enums.Role;
import uz.ser7er.chemreagentstore.service.AuthService;

@AllArgsConstructor
@Controller
@RequestMapping("/auth")
public class AuthController {
    private final AuthService authService;
    private final CustomUserDetailsService customUserDetailsService;

    @GetMapping("/login")
    public String goToLogin(Model model) {
        model.addAttribute("authobj", new AuthDTO());
        return "auth/loginPage";
    }

    @GetMapping("/registration")
    public String goToRegistration(Model model) {
        model.addAttribute("authObj", new AuthDTO());
        return "auth/registration";
    }

    @PostMapping("/registration/save")
    public String registration(@ModelAttribute AuthDTO authDTO) {
        Boolean result = authService.register(authDTO);
        if (result) return "redirect:/auth/login";
        return "auth/registration";
    }

    @GetMapping("/failed")
    public String getFailed(Model model) {
        model.addAttribute("authobj", new AuthDTO());
        return "auth/loginPage";
    }

    @PostMapping("/success")
    public String getSuccess() {
        if (customUserDetailsService.getUserRole().equals(Role.ROLE_ADMIN))
            return "redirect:/dashboard";
        else return "redirect:/index/chemicals";
    }

    @GetMapping("/logout")
    public String logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        if (authentication != null){
            new SecurityContextLogoutHandler().logout(request, response, authentication);
        }
        return "redirect:/index/chemicals";
    }
}
