package uz.ser7er.chemreagentstore.dto;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CategoryDto {
    private Integer id;
    private String title;
    private String imagePath;
    private Integer parentId;
    private Boolean enabled;
}
