package uz.ser7er.chemreagentstore.dto;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FilterDto {
    private String sortField;
    private String sortDir;
}
