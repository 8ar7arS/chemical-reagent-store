package uz.ser7er.chemreagentstore.dto.wrap;

import lombok.*;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DataTable<T> {
    private List<T> data;
    private int totalPage;
    private long totalItems;
}
