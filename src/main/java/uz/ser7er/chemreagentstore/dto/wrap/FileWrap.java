package uz.ser7er.chemreagentstore.dto.wrap;

import lombok.*;
import org.springframework.web.multipart.MultipartFile;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FileWrap {
    private MultipartFile file;
}
