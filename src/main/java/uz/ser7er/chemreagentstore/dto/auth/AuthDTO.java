package uz.ser7er.chemreagentstore.dto.auth;

import lombok.Data;

@Data
public class AuthDTO {
    private String username;
    private String lastname;
    private String email;
    private String password;
}
