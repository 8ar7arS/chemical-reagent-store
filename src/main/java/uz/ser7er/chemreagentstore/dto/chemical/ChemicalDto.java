package uz.ser7er.chemreagentstore.dto.chemical;

import lombok.*;
import uz.ser7er.chemreagentstore.entities.Category;
import uz.ser7er.chemreagentstore.entities.Manufacturer;
import uz.ser7er.chemreagentstore.entities.enums.UnitType;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ChemicalDto {
    private Integer id;
    private String name;
    private String description;
    private String imgUrl;
    private Integer manufacturerId;
    private Manufacturer manufacturer;
    private String manufacturerName;
    private Integer categoryId;
    private Category category;
    private String categoryTitle;
    private UnitType unitType;
    private Integer unitTypeId;
    private BigDecimal availableAmount;
    private BigDecimal price;
    private Integer cartItemId;
}
