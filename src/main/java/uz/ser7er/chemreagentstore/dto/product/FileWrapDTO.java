package uz.ser7er.chemreagentstore.dto.product;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FileWrapDTO {
    private MultipartFile multipartFile;
}
