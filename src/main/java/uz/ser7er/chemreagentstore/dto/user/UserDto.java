package uz.ser7er.chemreagentstore.dto.user;

import lombok.*;
import uz.ser7er.chemreagentstore.entities.Address;
import uz.ser7er.chemreagentstore.entities.enums.Role;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserDto {
    private Integer id;
    private String userName;
    private String password;
    private String email;
    private Role role;
    private Integer roleOrdinal;
    private Set<Address> address;
    private Integer cartId;
}
