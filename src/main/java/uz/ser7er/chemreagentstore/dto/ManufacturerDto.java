package uz.ser7er.chemreagentstore.dto;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ManufacturerDto {
    private Integer id;
    private String name;
}
