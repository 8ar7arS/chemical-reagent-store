package uz.ser7er.chemreagentstore.dto.attach;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AttachDTO {
    private String id;
    private String path;
    private String url;
    private String originalName;
    private LocalDateTime createdDate;
}
