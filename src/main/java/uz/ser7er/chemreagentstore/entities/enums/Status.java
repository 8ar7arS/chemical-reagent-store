package uz.ser7er.chemreagentstore.entities.enums;

public enum Status {
    CREATED,
    ACTIVE,
    DELETED,
    PASSIVE,
    UPDATED
}
