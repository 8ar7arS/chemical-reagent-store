package uz.ser7er.chemreagentstore.entities.enums;

public enum UnitType {
    MG, G, KG,
    ML, L,
    ONE_UNIT, OTHER
}
