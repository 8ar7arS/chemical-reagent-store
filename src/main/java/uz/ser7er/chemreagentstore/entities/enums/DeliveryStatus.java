package uz.ser7er.chemreagentstore.entities.enums;

public enum DeliveryStatus {
    IN_PROGRESS,
    SHIPPED,
    CANCELED,
    DELIVERED
}
