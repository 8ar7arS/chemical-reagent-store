package uz.ser7er.chemreagentstore.entities.enums;

public enum Role {
    ROLE_CUSTOMER,
    ROLE_ADMIN
}
