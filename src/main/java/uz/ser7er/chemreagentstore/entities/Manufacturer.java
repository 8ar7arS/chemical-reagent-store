package uz.ser7er.chemreagentstore.entities;

import jakarta.persistence.*;
import lombok.*;
import org.springframework.beans.BeanUtils;
import uz.ser7er.chemreagentstore.dto.ManufacturerDto;
import uz.ser7er.chemreagentstore.entities.baseentity.ModifierEntity;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@ToString
@Entity
@Table(name = "manufacturer")
public class Manufacturer extends ModifierEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name", nullable = false)
    private String name;

    @OneToMany(mappedBy = "manufacturer")
    @ToString.Exclude
    private List<Chemical> chemicals;

    public ManufacturerDto getDto() {
        ManufacturerDto dto = new ManufacturerDto();
        BeanUtils.copyProperties(this, dto);
        return dto;
    }
}
