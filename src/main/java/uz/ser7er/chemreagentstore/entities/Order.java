package uz.ser7er.chemreagentstore.entities;

import jakarta.persistence.*;
import lombok.*;
import uz.ser7er.chemreagentstore.entities.enums.DeliveryStatus;

import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Entity
@Table(name = "orders")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "user_id", insertable = false, updatable = false)
    private User user;

    @Column(name = "created_at")
    private LocalDate createdAt = LocalDate.now();

    @Column(name = "order_status")
    @Enumerated(EnumType.STRING)
    private DeliveryStatus deliveryStatus;
}
