package uz.ser7er.chemreagentstore.entities;

import jakarta.persistence.*;
import lombok.*;

import java.math.BigDecimal;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Entity
@Table(name = "cart_item")
public class CartItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "amount")
    private BigDecimal amount;

    @Column(name = "chemical_id") //???
    private Integer chemicalId;

    @OneToOne(fetch = FetchType.LAZY) //, cascade = CascadeType.ALL
    @JoinColumn(name = "product_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ToString.Exclude
    private Chemical chemical; // product

    @ManyToOne(fetch = FetchType.LAZY) //, cascade = CascadeType.ALL
    @JoinColumn(name = "card_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ToString.Exclude
    private Cart cart; // korzinka
}
