package uz.ser7er.chemreagentstore.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Table(name = "attach")
public class AttachEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;
    private String path;
    @Column
    private String extension;
    @Column
    private String originalName;
    @Column
    private Long size;
    @Column
    private LocalDateTime createdDate;
}
