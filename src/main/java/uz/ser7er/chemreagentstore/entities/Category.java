package uz.ser7er.chemreagentstore.entities;

import jakarta.persistence.*;
import lombok.*;
import org.springframework.beans.BeanUtils;
import uz.ser7er.chemreagentstore.dto.CategoryDto;
import uz.ser7er.chemreagentstore.entities.baseentity.ModifierEntity;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@ToString
@Entity
@Table(name = "category")
public class Category extends ModifierEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String title;

    @Column(name = "image_path")
    private String imagePath;

    @Column(name = "enabled")
    private boolean enabled = true;

    @Column(name = "parent_id")
    private Integer parentId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_id", insertable = false, updatable = false)
    @ToString.Exclude
    private Category parent;

    @OneToMany(mappedBy = "parent")
    @OrderBy("title asc")
    @ToString.Exclude
    private Set<Category> children = new HashSet<>(); // bu column ortiqcha, parentId si null bolsa osha katta bo'loradi, categoriyaId si 1 ga teng bosin desa 2 chi id da tugan row parrend id si 1 ga teng bolsa va 2 chi row id qaysidur row di parent id siga teng bo'lsa shuni olib chiqib kerak

    @OneToMany(mappedBy = "category")//, cascade = {CascadeType.ALL, CascadeType.PERSIST}
    @ToString.Exclude
    private List<Chemical> chemicals;

    @Column(name = "all_parent_ids")
    @ToString.Exclude
    private String ParentIdes;

    public CategoryDto getDto() {
        CategoryDto dto = new CategoryDto();
        BeanUtils.copyProperties(this, dto);
        return dto;
    }
}
