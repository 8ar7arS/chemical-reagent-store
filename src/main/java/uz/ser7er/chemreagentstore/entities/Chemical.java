package uz.ser7er.chemreagentstore.entities;

import jakarta.persistence.*;
import lombok.*;
import org.springframework.beans.BeanUtils;
import uz.ser7er.chemreagentstore.dto.chemical.ChemicalDto;
import uz.ser7er.chemreagentstore.entities.baseentity.ModifierEntity;
import uz.ser7er.chemreagentstore.entities.enums.UnitType;

import java.math.BigDecimal;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "chemical")
public class Chemical extends ModifierEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "image")
    private String imageUrl;

    @Column(name = "manufacturer_id")
    private Integer manufacturerId;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "manufacturer_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ToString.Exclude
    private Manufacturer manufacturer;

    @Column(name = "category_id")
    private Integer categoryId;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "category_id", insertable = false, updatable = false)
    @ToString.Exclude
    private Category category;

    @Column(name = "unit_type", nullable = false)
    @Enumerated(EnumType.STRING)
    private UnitType unitType;

    @Column(name = "available_amount", nullable = false)
    private BigDecimal availableAmount;

    @Column(name = "price")
    private BigDecimal price;

    @Column(name = "cart_item_id")
    private Integer cartItemId;

    @OneToOne(mappedBy = "chemical")
    @JoinColumn(name = "cart_item_id", referencedColumnName = "id", insertable = false, updatable = false)
    private CartItem cartItem;

    public ChemicalDto getDto() {
        ChemicalDto dto = new ChemicalDto();
        BeanUtils.copyProperties(this, dto);
        return dto;
    }

}
