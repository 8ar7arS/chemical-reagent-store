package uz.ser7er.chemreagentstore.entities;

import jakarta.persistence.*;
import lombok.*;
import uz.ser7er.chemreagentstore.entities.baseentity.ModifierEntity;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Entity
@Table(name = "user_info")
public class UserInfo extends ModifierEntity {
    @Id
    @Column(name = "user_info_id", nullable = false)
    private Integer userId;

    @MapsId
    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_info_id", insertable = false, updatable = false)
    @ToString.Exclude
    private User user;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "phone")
    private String phone;
}
