package uz.ser7er.chemreagentstore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChemReagentStoreApplication {
    public static void main(String[] args) {
        SpringApplication.run(ChemReagentStoreApplication.class, args);
    }
}
