package uz.ser7er.chemreagentstore.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
public class SecurityConfig {

    private UserDetailsService userDetailsService;

    @Autowired
    public SecurityConfig(UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @Bean
    public AuthenticationProvider authenticationProvider() {
        final DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
        authenticationProvider.setUserDetailsService(userDetailsService);
        authenticationProvider.setPasswordEncoder(passwordEncoder());
        return authenticationProvider;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        // authorization
        http.headers().frameOptions().disable();
        http.csrf().disable()
                .authorizeHttpRequests()
                .requestMatchers("/auth/failed").permitAll()
                .requestMatchers("/auth/**").permitAll()
                .requestMatchers("/img/**").permitAll()
                .requestMatchers("/css/**").permitAll()
                .requestMatchers("/js/**").permitAll()
                .requestMatchers("/vendor/**").permitAll()
                .requestMatchers("../resources").permitAll()
                .requestMatchers("/templates/fragments/**").permitAll()
                .requestMatchers("/h2-console/**").permitAll()
                .requestMatchers("/index/**").permitAll()
                .requestMatchers("/index/details/**").permitAll()
                .requestMatchers("/index/chemicals/**").permitAll()
                .requestMatchers("/index/checkout/**").permitAll()
                .requestMatchers("/resource/**").permitAll()
                .requestMatchers("/attach/**").permitAll()
                .requestMatchers("", "/**").hasRole("ADMIN")
                .and()
                .formLogin()
                .loginPage("/auth/login").permitAll()
                .loginProcessingUrl("/auth/login_process")
                .successForwardUrl("/auth/success")
                .failureUrl("/auth/failed")
                .and()
                .logout()
                .logoutSuccessUrl("/index/chemicals").permitAll()
                .and()
                .authorizeHttpRequests()
                .anyRequest()
                .authenticated();
        return http.build();
    }

    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration config) throws Exception {
        return config.getAuthenticationManager();
    }
}
