package uz.ser7er.chemreagentstore.service;

import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.ser7er.chemreagentstore.config.CustomUserDetailsService;
import uz.ser7er.chemreagentstore.dto.attach.AttachDTO;
import uz.ser7er.chemreagentstore.dto.chemical.ChemicalDto;
import uz.ser7er.chemreagentstore.dto.product.FileWrapDTO;
import uz.ser7er.chemreagentstore.dto.wrap.DataTable;
import uz.ser7er.chemreagentstore.entities.Chemical;
import uz.ser7er.chemreagentstore.entities.enums.Status;
import uz.ser7er.chemreagentstore.entities.enums.UnitType;
import uz.ser7er.chemreagentstore.repository.chemical.ChemicalRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@AllArgsConstructor
@Service
public class ChemicalService {
    private final ChemicalRepository chemicalRepository;
    private final CustomUserDetailsService customUserDetailsService;
    private final AttachService attachService;


    @Transactional
    public DataTable<ChemicalDto> getList(int page, int size, String sortField, String dir, ChemicalDto filterDto) {
        Sort sort = null;
        String field;

        if (sortField == null) sortField = "id";

        if (sortField.equalsIgnoreCase("categoryTitle")) field = "category.title";
        else if (sortField.equalsIgnoreCase("manufacturerName")) field = "manufacturer.name";
        else field = sortField;

        if (dir != null && !dir.isEmpty() && !dir.isBlank()) {
            if (dir.equalsIgnoreCase("asc")) sort = Sort.by(Sort.Direction.ASC, field);
            else sort = Sort.by(Sort.Direction.DESC, field);
        } else sort = Sort.by(Sort.Direction.ASC, "id");

        Pageable pageable = PageRequest.of(page, size, sort);
        Page<Chemical> chemicalPage = chemicalRepository
                                            .getChemicalWithFilter(
                                            filterDto.getName(),
                                            filterDto.getCategoryId(),
                                            filterDto.getManufacturerId(),
                                            filterDto.getPrice(),
                                            filterDto.getUnitTypeId() != null ? UnitType.values()[filterDto.getUnitTypeId()] : null,
                                            Status.DELETED, pageable);
        List<Chemical> content = chemicalPage.getContent();


        DataTable<ChemicalDto> table = new DataTable<>();
        table.setData(content.stream().map(chemical -> {
            ChemicalDto dto = chemical.getDto();
            dto.setCategory(chemical.getCategory());
            dto.setManufacturer(chemical.getManufacturer());
            dto.setImgUrl(chemical.getImageUrl());
            return dto;
        }).collect(Collectors.toList()));
        table.setTotalPage(chemicalPage.getTotalPages());
        table.setTotalItems(chemicalPage.getTotalElements());

        return table;
    }

    public DataTable<ChemicalDto> getList(int page, int size) {

        Sort sort = Sort.by(Sort.Direction.ASC, "id");

        Pageable pageable = PageRequest.of(page, size, sort);
        Page<Chemical> chemicalPage = chemicalRepository.findByStatusNot(Status.DELETED, pageable);
        List<Chemical> content = chemicalPage.getContent();

        DataTable<ChemicalDto> table = new DataTable<>();
        table.setData(content.stream().map(chemical -> {
            ChemicalDto dto = chemical.getDto();
            dto.setCategory(chemical.getCategory());
            dto.setManufacturer(chemical.getManufacturer());
            dto.setImgUrl(chemical.getImageUrl());
            return dto;
        }).collect(Collectors.toList()));
        table.setTotalPage(chemicalPage.getTotalPages());
        table.setTotalItems(chemicalPage.getTotalElements());

        return table;
    }

    public Boolean create(ChemicalDto chemicalDto, FileWrapDTO fileWrapDTO) {
        Chemical chemical = new Chemical();
        chemical.setName(chemicalDto.getName());
        chemical.setAvailableAmount(chemicalDto.getAvailableAmount());
        chemical.setPrice(chemicalDto.getPrice());
        chemical.setManufacturerId(chemicalDto.getManufacturerId());
        chemical.setCategoryId(chemicalDto.getCategoryId());
        chemical.setDescription(chemicalDto.getDescription());
        chemical.setUnitType(UnitType.values()[chemicalDto.getUnitTypeId()]);
        chemical.setCreatedBy(customUserDetailsService.getUserId());
        chemical.setCreatedDate(LocalDateTime.now());

        if (fileWrapDTO.getMultipartFile() != null && !fileWrapDTO.getMultipartFile().isEmpty()) {
            if (!Objects.equals(fileWrapDTO.getMultipartFile().getOriginalFilename(), "")) {
                AttachDTO upload = attachService.upload(fileWrapDTO.getMultipartFile());
                chemical.setImageUrl(upload.getId());
            }
        }
        chemicalRepository.save(chemical);
        return true;
    }

    @Transactional
    public ChemicalDto getById(Integer chemicalId) {
        Optional<Chemical> optional = chemicalRepository.findByIdAndStatusNot(chemicalId, Status.DELETED);
        return optional.map(chemical -> {
            ChemicalDto dto = chemical.getDto();
            dto.setCategory(chemical.getCategory());
            dto.setManufacturer(chemical.getManufacturer());
            dto.setImgUrl(chemical.getImageUrl());
            return dto;
        }).orElse(null);
    }

    public Boolean edit(ChemicalDto chemicalDto, FileWrapDTO fileWrapDTO) {
        Optional<Chemical> optional = chemicalRepository.findByIdAndStatusNot(chemicalDto.getId(), Status.DELETED);
        if (optional.isPresent()) {
            Chemical chemical = optional.get();
            chemical.setName(chemicalDto.getName());
            chemical.setAvailableAmount(chemicalDto.getAvailableAmount());
            chemical.setPrice(chemicalDto.getPrice());
            chemical.setCategoryId(chemicalDto.getCategoryId());
            chemical.setManufacturerId(chemicalDto.getManufacturerId());
            chemical.setDescription(chemicalDto.getDescription());
            chemical.setUpdatedBy(customUserDetailsService.getUserId());
            chemical.setUpdatedDate(LocalDateTime.now());

            if (fileWrapDTO.getMultipartFile() != null && !fileWrapDTO.getMultipartFile().isEmpty()) {
                if (!Objects.equals(fileWrapDTO.getMultipartFile().getOriginalFilename(), "")) {
                    AttachDTO upload = attachService.upload(fileWrapDTO.getMultipartFile());
                    chemical.setImageUrl(upload.getId()); //  todo url bolishi kerak
                }
            }
            chemicalRepository.save(chemical);
            return true;
        }
        return false;
    }

    public Long chemicalCount() {
        return chemicalRepository.countByStatusNot(Status.DELETED).orElse(null);
    }

    public Boolean delete(Integer chemicalId) {
        Optional<Chemical> optional = chemicalRepository.findByIdAndStatusNot(chemicalId, Status.DELETED);
        if (optional.isPresent()) {
            Chemical chemical = optional.get();
            chemical.setUpdatedDate(LocalDateTime.now());
            chemical.setUpdatedBy(customUserDetailsService.getUserId());
            chemical.setStatus(Status.DELETED);
            chemicalRepository.save(chemical);
            return true;
        }
        return false;
    }
}
