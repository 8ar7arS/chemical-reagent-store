package uz.ser7er.chemreagentstore.service;

import lombok.AllArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import uz.ser7er.chemreagentstore.dto.auth.AuthDTO;
import uz.ser7er.chemreagentstore.entities.User;
import uz.ser7er.chemreagentstore.entities.enums.Role;
import uz.ser7er.chemreagentstore.repository.user.UserRepository;

import java.time.LocalDateTime;
import java.util.Optional;

@AllArgsConstructor
@Service
public class AuthService {
    private final UserRepository userRepository;

    public Boolean register(AuthDTO authDTO) {
        Optional<User> optional = userRepository.findByEmail(authDTO.getEmail());
        if (optional.isEmpty()) {
            User user = new User();
            user.setUserName(authDTO.getUsername());
            user.setEmail(authDTO.getEmail());
            user.setPassword(new BCryptPasswordEncoder().encode(authDTO.getPassword()));
            user.setRole(Role.ROLE_CUSTOMER);
            user.setCreatedDate(LocalDateTime.now());
            userRepository.save(user);
            return true;
        }
        return false;
    }
}
