package uz.ser7er.chemreagentstore.service;

import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import uz.ser7er.chemreagentstore.config.CustomUserDetailsService;
import uz.ser7er.chemreagentstore.dto.ManufacturerDto;
import uz.ser7er.chemreagentstore.dto.wrap.DataTable;
import uz.ser7er.chemreagentstore.entities.Manufacturer;
import uz.ser7er.chemreagentstore.entities.enums.Status;
import uz.ser7er.chemreagentstore.repository.manufacturer.ManufacturerRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@AllArgsConstructor
@Service
public class ManufacturerService {
    private final ManufacturerRepository manufacturerRepository;
    private final CustomUserDetailsService customUserDetailsService;

    public DataTable<ManufacturerDto> getListWithPaging(int page, int size, String sortField, String sortDir, ManufacturerDto filterDto) {
        Sort sort = Sort.by(sortDir.equalsIgnoreCase("asc") ? Sort.Direction.ASC : Sort.Direction.DESC, sortField);
        Pageable pageable = PageRequest.of(page, size, sort);

        Page<Manufacturer> manufacturerPage = manufacturerRepository.getManufacturerList(filterDto.getName(), pageable);
        List<ManufacturerDto> manufaturerDtoList = manufacturerPage.getContent().stream().map(Manufacturer::getDto).collect(Collectors.toList());

        DataTable<ManufacturerDto> table = new DataTable<>();
        table.setData(manufaturerDtoList);
        table.setTotalPage(manufacturerPage.getTotalPages());
        table.setTotalItems(manufacturerPage.getTotalElements());

        return table;
    }

    public List<ManufacturerDto> getList() {
        return manufacturerRepository.findAllByStatusNot(Status.DELETED).stream().map(Manufacturer::getDto).collect(Collectors.toList());
    }

    public Boolean create(ManufacturerDto manufacturerDto) {
        Optional<Manufacturer> optional = manufacturerRepository.findByNameAndStatusNot(manufacturerDto.getName(), Status.DELETED);
        if (optional.isPresent()) return false;

        Manufacturer manufacturer = new Manufacturer();
        manufacturer.setName(manufacturerDto.getName());
        manufacturer.setCreatedBy(customUserDetailsService.getUserId());
        manufacturer.setCreatedDate(LocalDateTime.now());
        manufacturerRepository.save(manufacturer);
        return true;
    }

    public ManufacturerDto getById(Integer manufacturerId) {
        Optional<Manufacturer> optional = manufacturerRepository.findByIdAndStatusNot(manufacturerId, Status.DELETED);
        if (optional.isPresent()) {
            Manufacturer manufacturer = optional.get();
            return manufacturer.getDto();
        }
        return null;
    }

    public Boolean update(ManufacturerDto manufacturerDto) {
        Optional<Manufacturer> optional = manufacturerRepository.findByIdAndStatusNot(manufacturerDto.getId(), Status.DELETED);
        if (optional.isPresent()) {
            Manufacturer manufacturer = optional.get();
            manufacturer.setName(manufacturerDto.getName());
            manufacturer.setUpdatedBy(customUserDetailsService.getUserId());
            manufacturer.setUpdatedDate(LocalDateTime.now());
            manufacturerRepository.save(manufacturer);
            return true;
        }
        return false;
    }

    public Boolean delete(Integer manufacturerId) {
        Optional<Manufacturer> optional = manufacturerRepository.findByIdAndStatusNot(manufacturerId, Status.DELETED);
        if (optional.isPresent()) {
            Manufacturer manufacturer = optional.get();
            manufacturer.setStatus(Status.DELETED);
            manufacturer.setUpdatedBy(customUserDetailsService.getUserId());
            manufacturer.setUpdatedDate(LocalDateTime.now());
            manufacturerRepository.save(manufacturer);
            return true;
        }
        return false;
    }

    public Long ManufacturerCount() {
        return manufacturerRepository.countByStatusNot(Status.DELETED).orElse(null);
    }
}
