package uz.ser7er.chemreagentstore.service;

import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import uz.ser7er.chemreagentstore.dto.CategoryDto;
import uz.ser7er.chemreagentstore.dto.wrap.DataTable;
import uz.ser7er.chemreagentstore.entities.Category;
import uz.ser7er.chemreagentstore.entities.enums.Status;
import uz.ser7er.chemreagentstore.repository.category.CategoryRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@AllArgsConstructor
@Service
public class CategoryService {
    private final CategoryRepository categoryRepository;

    public DataTable<CategoryDto> getListWithPaging(int page, int size, String sortField, String sortDir, CategoryDto filterDto) {
        Sort sort = Sort.by(sortDir.equalsIgnoreCase("asc") ? Sort.Direction.ASC : Sort.Direction.DESC, sortField);
        Pageable pageable = PageRequest.of(page, size, sort);

        Page<Category> categoryPage = categoryRepository.getCategoryList(filterDto.getTitle(), pageable);
        List<CategoryDto> categoryDtoList = categoryPage
                                            .getContent()
                                            .stream()
                                            .map(Category::getDto)
                                            .collect(Collectors.toList());

        DataTable<CategoryDto> table = new DataTable<>();
        table.setData(categoryDtoList);
        table.setTotalPage(categoryPage.getTotalPages());
        table.setTotalItems(categoryPage.getTotalElements());

        return table;
    }

    public List<CategoryDto> getList() {
        return categoryRepository
                .findAllByStatusNot(Status.DELETED)
                .stream().map(Category::getDto)
                .collect(Collectors.toList());
    }

    public void create(CategoryDto categoryDto) {
        Category category = Category.builder()
                .title(categoryDto.getTitle())
                .enabled(categoryDto.getEnabled())
                .parentId(categoryDto.getParentId())
                .imagePath(categoryDto.getImagePath())
                .build();
        categoryRepository.save(category);
    }

    public CategoryDto getById(Integer categoryId) {
        Optional<Category> optional = categoryRepository.findByIdAndStatusNot(categoryId, Status.DELETED);
        if (optional.isPresent()) {
            Category entity = optional.get();
            return entity.getDto();
        }
        return null;
    }

    public Boolean update(CategoryDto categoryDto) {
        Optional<Category> optional = categoryRepository.findByIdAndStatusNot(categoryDto.getId(), Status.DELETED);
        if (optional.isPresent()) {
            Category category = optional.get();
            category.setTitle(categoryDto.getTitle());
            category.setParentId(categoryDto.getParentId());
            category.setEnabled(categoryDto.getEnabled());
            categoryRepository.save(category);
            return true;
        }
        return false;
    }

    public Boolean delete(Integer categoryId) {
        Optional<Category> optional = categoryRepository.findByIdAndStatusNot(categoryId, Status.DELETED);
        if (optional.isPresent()) {
            Category category = optional.get();
            category.setStatus(Status.DELETED);
            categoryRepository.save(category);
            return true;
        }
        return false;
    }

    public Long categoryCount() {
        return categoryRepository.countByStatusNot(Status.DELETED).orElse(null);
    }
}
