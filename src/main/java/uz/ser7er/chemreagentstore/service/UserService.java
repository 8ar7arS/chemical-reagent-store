package uz.ser7er.chemreagentstore.service;

import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.ser7er.chemreagentstore.config.CustomUserDetailsService;
import uz.ser7er.chemreagentstore.dto.user.UserDto;
import uz.ser7er.chemreagentstore.dto.wrap.DataTable;
import uz.ser7er.chemreagentstore.entities.User;
import uz.ser7er.chemreagentstore.entities.enums.Role;
import uz.ser7er.chemreagentstore.entities.enums.Status;
import uz.ser7er.chemreagentstore.repository.user.UserRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@AllArgsConstructor
@Service
public class UserService {
    private final UserRepository userRepository;
    private final CustomUserDetailsService customUserDetailsService;

    @Transactional
    public DataTable<UserDto> getList(int page, int size, String sortField, String sortDir, UserDto filterDto) {
        Sort sort = null;
        if (sortDir != null && !sortDir.isEmpty() && !sortDir.isBlank()) {
            if (sortDir.equalsIgnoreCase("asc")) sort = Sort.by(Sort.Direction.ASC, sortField);
            else sort = Sort.by(Sort.Direction.DESC, sortField);
        } else sort = Sort.by(Sort.Direction.ASC, "id");

        Pageable pageable = PageRequest.of(page, size, sort);
        Page<User> userPage = userRepository.getList(filterDto.getUserName(), pageable);
        List<User> content = userPage.getContent();

        DataTable<UserDto> table = new DataTable<>();
        table.setData(content.stream().map(user -> {
            UserDto dto = user.getDto();
            dto.setAddress(user.getAddresses());
            return dto;
        }).collect(Collectors.toList()));

        table.setTotalPage(userPage.getTotalPages());
        table.setTotalItems(userPage.getTotalElements());
        return table;
    }

    @Transactional
    public UserDto getById(Integer userId) {
        Optional<User> optional = userRepository.findByIdAndStatusNot(userId, Status.DELETED);
        if (optional.isPresent()) {
            User user = optional.get();
            UserDto dto = user.getDto();
            dto.setRole(user.getRole());
            dto.setRoleOrdinal(user.getRole().ordinal());
            dto.setAddress(user.getAddresses());
            dto.setPassword(null);
            return dto;
        }
        return null;
    }

    public Boolean edit(UserDto userDto) {
        Optional<User> optional = userRepository.findByIdAndStatusNot(userDto.getId(), Status.DELETED);
        if (optional.isPresent()) {
            User user = optional.get();
            user.setUserName(userDto.getUserName());
            user.setEmail(userDto.getEmail());
            user.setRole(Role.values()[userDto.getRoleOrdinal()]);
            if (!userDto.getPassword().isEmpty())
                user.setPassword(new BCryptPasswordEncoder().encode(userDto.getPassword()));
            user.setUpdatedBy(customUserDetailsService.getUserId());
            user.setUpdatedDate(LocalDateTime.now());
            userRepository.save(user);
            return true;
        }
        return false;
    }
}
