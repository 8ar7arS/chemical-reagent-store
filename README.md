### In order to create some dummy data on DB, please got to: http://localhost:8080/index/init
#### it will automatically create some data

#### _some URIs to check:_
- _dashboard:_ http://localhost:8080/dashboard
- _login:_ admin@mail.ru
- _password:_ 123
-
- _main page:_ http://localhost:8080/index/manufacturers




# Chemical reagents online store
### Class Diagrams
![alt text](src/main/resources/classDiagrams.jpeg)

### _High-Level Overview_
- _Type of App:_ E-commerce platform specializing in selling chemical reagents.
- _For Whom:_ Scientists, researchers, laboratories, and educational institutions.
- _Satisfies Needs:_ Provides a convenient platform to purchase a wide range of chemical reagents, ensuring quality, reliability, and compliance with safety standards.

### Requirements for the Chemical Reagents Online Store:
#### _Functional Requirements:_
- _Admin Panel:_ Provide an interface for administrators to manage products, orders, users, and other aspects of the system.
- _Product Catalog:_ Display a list of available chemical reagents with detailed descriptions, prices, and availability status.
- _Shopping Cart:_ Enable users to add/remove items, view cart contents, and proceed to checkout.
- _Order Management:_ Allow users to view order history, track order status, and manage their orders.
- _Search and Filter:_ Allow users to search for specific products and filter results based on various criteria.
- ##### _Optionally:_
- _User Registration and Authentication:_ Allow users to create accounts and log in securely.
- _Account Management:_ Allow users to update their profile information, manage addresses, and change password.
- _Payment Gateway Integration:_ Provide a secure payment process for purchasing items.

#### _Non-functional Requirements:_
- _Security:_ Ensure the system is secure and compliant with data protection regulations.
- _Performance:_ Ensure the system can handle a large number of users and transactions efficiently.
- _Usability:_ Design a user-friendly interface that is easy to navigate and understand.
- _Scalability:_ Design the system to easily scale up to accommodate future growth.
- _Reliability:_ Ensure the system is reliable and available 24/7 with minimal downtime.

### Use Cases for the System:
- _Browse Products:_ User can view the list of available chemical reagents.
- _Search Products_: User can search for specific products based on name or category.
- _View Product Details:_ User can view detailed information about a specific product.
- _Add to Cart:_ User can add a product to the shopping cart.
- _Remove from Cart:_ User can remove a product from the shopping cart.
- _View Cart:_ User can view the contents of the shopping cart.
- _Update Cart:_ User can update the quantity of a product in the shopping cart.
- _Register:_ User can create a new account.
- _Login:_ User can log in to their account.
- _View Order History:_ User can view their past order history.
- _Track Order:_ User can track the status of their current orders.
- _Manage Account:_ User can update their profile information and manage their addresses.
- _Manage Products (Admin)_: Admin can add, edit, or delete products from the catalog.
- _Manage Orders (Admin):_ Admin can view and manage orders placed by users.
- _Manage Users (Admin):_ Admin can view and manage user accounts.

### Objects, Classes, and Relationships:

- _Objects/Classes:_ User, Product, ShoppingCart, Order, Payment, Admin
- _Relationships:_
  -- User has a ShoppingCart
  -- User places Orders
  -- Order contains Products
  -- Payment is associated with an Order
  -- Admin manages Products, Orders, and Users

### Class Diagram:

|            User              |
|------------------------------|
| - id: int                    |
| - name: string               |
| - email: string              |
| - password: string           |
| - address: string            |
| - isAdmin: boolean           |
| + register()                 |
| + login()                    |
| + updateProfile()            |
| + viewOrderHistory()         |
| + order()                    |
| + manageAddress()            |

|           Product            |
|------------------------------|
| - id: int                    |
| - name: string               |
| - description: string        |
| - price: decimal             |
| - stock: int                 |
| - category: string           |
| + addToCart()                |
| + removeFromCart()           |
| + updateStock()              |



|        ShoppingCart          |
|------------------------------|
| - id: int                    |
| - items: List<Product>       |
| + addItem()                  |
| + removeItem()               |
| + updateItemQuantity()       |
| + checkout()                 |

|           Order              |
|------------------------------|
| - id: int                    |
| - items: List<Product>       |
| - totalAmount: decimal       |
| - status: string             |
| + addProduct()               |
| + removeProduct()            |
| + updateStatus()             |


|          Payment             |
|------------------------------|
| - id: int                    |
| - orderId: int               |
| - amount: decimal            |
| - paymentStatus: string      |
| + processPayment()           |

|           Admin              |
|------------------------------|
| - id: int                    |
| - name: string               |
| - email: string              |
| - password: string           |
| + addProduct()               |
| + editProduct()              |
| + deleteProduct()            |
| + viewOrders()               |
| + manageUsers()              |
